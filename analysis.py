import json

from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from textblob import TextBlob
from elasticsearch import Elasticsearch
from datetime import datetime
from geopy.geocoders import Nominatim

# import twitter developer keys and tokens
from config import *

# create instance of elasticsearch
es = Elasticsearch([elasticsearch_uri])

# create instance of Notinatim
geolocator = Nominatim()


class TweetStreamListener(StreamListener):
    # on success
    def on_data(self, data):

        # decode json
        dict_data = json.loads(data)

        # parse string location to geolocation
        location = geolocator.geocode(dict_data["user"]["location"], timeout=None)

        # check if location has been parsed and store lantitude and longtitude
        latitude = 00.0000000
        longtitude = 00.0000000

        if location is not None:
            latitude = location.latitude
            longtitude = location.longitude
            print "latitude: %s and longtitude: %s" % (latitude, longtitude)

        # pass tweet into TextBlob
        tweet = TextBlob(dict_data["text"])

        # output sentiment polarity
        print "polarity: %s" % (tweet.sentiment.polarity)

        # determine if sentiment is positive, negative, or neutral
        if tweet.sentiment.polarity < 0:
            sentiment = "negative"
        elif tweet.sentiment.polarity == 0:
            sentiment = "neutral"
        else:
            sentiment = "positive"

        # print tweet sentiment
        print "sentiment: %s" % (sentiment)

        # print subjectivity
        print "subjectivity: %s" % (tweet.sentiment.subjectivity)

        # fix the timestamp format
        timestamp = datetime.strptime(dict_data["created_at"].replace("+0000 ", ""), "%a %b %d %H:%M:%S %Y").isoformat()
        print "timestamp: %s" % (timestamp) + "\n"

        # add text and sentiment info to elasticsearch
        es.index(index="analysis",
                 doc_type="sentiment",
                 body={"author": dict_data["user"]["screen_name"],
                       "location": {
                           "lat": latitude,
                           "lon": longtitude
                       },
                       "followers": dict_data["user"]["followers_count"],
                       "timestamp": timestamp,
                       "message": dict_data["text"],
                       "polarity": tweet.sentiment.polarity,
                       "subjectivity": tweet.sentiment.subjectivity,
                       "sentiment": sentiment
                       })
        return True

    # on failure
    def on_error(self, status):
        print status


if __name__ == '__main__':
    # create instance of the tweepy tweet stream listener
    listener = TweetStreamListener()

    # set twitter keys/tokens
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    # create instance of the tweepy stream
    stream = Stream(auth, listener)

    # search twitter keyword
    stream.filter(track=[keyword])
