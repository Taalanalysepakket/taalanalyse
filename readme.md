# Pip install requirements
sudo pip install -r requirements.txt

#Docker build process
sudo docker build -rm -t=elasticsearch-kibana .

#Docker start
sudo docker run -d -p 9200:9200 -p 5601:5601 elasticsearch-kibana

#Docker container
sudo docker ps

#Stop Docker container
sudo docker kill <container(name or id)>

#Docker image list
sudo docker images

#Remove docker image
sudo docker rmi -f <image(name or id)>

#Enter docker container
sudo docker exec -it <container(name or id)> bash

#Run python analysis
python analysis.py
