# start with a base image
FROM ubuntu:latest

# Image maintainer
MAINTAINER Aram

# Proxy settings
ENV http_proxy="http://172.17.42.1:3128"
ENV https_proxy=$http_proxy
ENV HTTP_PROXY=$http_proxy
ENV HTTPS_PROXY=$http_proxy

# initial update
RUN apt-get update -q

# install Java
RUN apt-get install -yq wget default-jre-headless

# install Spark
RUN curl -s http://d3kbcqa49mib13.cloudfront.net/spark-1.5.1-bin-hadoop2.6.tgz | tar -xz -C /usr/local/
RUN cd /usr/local && ln -s spark-1.5.1-bin-hadoop2.6 spark
ENV SPARK_HOME /usr/local/spark
RUN mkdir $SPARK_HOME/yarn-remote-client
ADD yarn-remote-client $SPARK_HOME/yarn-remote-client

ENV ES_VERSION 1.7.2

# install elasticsearch
RUN cd /tmp && \
    wget -nv https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-${ES_VERSION}.tar.gz && \
    tar zxf elasticsearch-${ES_VERSION}.tar.gz && \
    rm -f elasticsearch-${ES_VERSION}.tar.gz && \
    mv /tmp/elasticsearch-${ES_VERSION} /elasticsearch


ENV KIBANA_VERSION 4.1.2

# install kibana
RUN cd /tmp && \
    wget -nv https://download.elastic.co/kibana/kibana/kibana-${KIBANA_VERSION}-linux-x64.tar.gz && \
    tar zxf kibana-${KIBANA_VERSION}-linux-x64.tar.gz && \
    rm -f kibana-${KIBANA_VERSION}-linux-x64.tar.gz && \
    mv /tmp/kibana-${KIBANA_VERSION}-linux-x64 /kibana

# start elasticsearch
CMD /elasticsearch/bin/elasticsearch -Des.logger.level=OFF & /kibana/bin/kibana -q

EXPOSE 9200 5601

# cUrl type index for geo mapping (geo_point)
RUN curl -XPUT http://localhost:9200/analysis -d '
{
    "mappings": {
        "sentiment": {
            "properties": {
                "sentiment": {"type": "string"},
                "subjectivity": {"type": "string"},
                "polarity": {"type": "string"},
                "message": {"type": "string"},
                "followers": {"type": "string"},
                "author": {"type": "string"},
                "timestamp": {"type": "date"},
                "location": {"type": "geo_point"}
            }
        }
    }
}
'
